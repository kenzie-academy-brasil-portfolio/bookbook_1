import React from "react";
import "../../cardsBooks/CardBooks.css";
import { Card } from "antd";

class CardBookAssessments extends React.Component {
  state = {
    booksFetch: [],
  };

  fetchAssessments = () => {
    const apiUrl = "https://ka-users-api.herokuapp.com/book_reviews";

    fetch(apiUrl, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("currentToken"),
      },
    })
      .then((res) => res.json())
      .then((res) => this.setState({ booksFetch: res }));
  };

  componentDidMount() {
    this.fetchAssessments();
  }

  render() {
    const { booksFetch } = this.state;
    return (
      <>
      <div className="CardsMother">
        {booksFetch.map((book, key) => {
          return (
            <Card
              loading={false}
              hoverable
              style={{ width: 441, height: 180, marginTop: 20 }}
              key={key}
            >
              <Card.Grid
                hoverable={false}
                style={{
                  width: 120,
                  height: 180,
                  padding: 0,
                  overflow: "hidden",
                }}
              >
                <img
                  style={{ height: 180 }}
                  alt="capa do livro"
                  src={book.image_url}
                />
              </Card.Grid>

              <Card.Grid
                hoverable={false}
                style={{
                  width: 320,
                  height: 180,
                  padding: 0,
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Card.Meta
                  style={{ width: "100%", margin: 0, padding: 14 }}
                  title={book.title}
                  description={book.review}
                />
                  Quem leu este livro? 
                <b>{book.creator.name}</b>
              </Card.Grid>
            </Card>
          );
        })}
        </div>
      </>
    );
  }
}

export default CardBookAssessments;
