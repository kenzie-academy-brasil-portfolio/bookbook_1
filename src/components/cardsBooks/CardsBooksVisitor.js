import React from "react";
import "./CardBooks.css";
import { Card, Modal, Button } from "antd";
import { useSelector } from "react-redux";
import { withRouter } from "react-router-dom";

const ReachableContext = React.createContext();

const config = {
  title: "Ops... Parece que você não está logado",
  content: (
    <div>
      <p>
        {" "}
        Para adicionar o livro a sua Wishlist, faça <a href="/login">Login</a>.
      </p>
      <p>
        Ainda não é um usuário? <a href="/forms">Cadastre-se. </a>
      </p>
    </div>
  ),
};

const CardBooksVisitor = () => {
  const [modal, contextHolder] = Modal.useModal();
  const booksMap = useSelector((state) => state.books.books);

  return (
    <div className="Container">
      <div className="CardsMother">
        {booksMap.map((book, key) => {
          return (
            <Card
              loading={false}
              hoverable
              style={{ width: 458, height: 180, marginTop: 20 }}
              key={key}
            >
              <Card.Grid
                hoverable={false}
                style={{
                  width: 136,
                  height: 180,
                  padding: 0,
                }}
              >
                <Card style={{ overflow: "hidden" }}>
                  <img
                    style={{ height: 180 }}
                    alt="capa do livro"
                    src={
                      book.volumeInfo.imageLinks &&
                      book.volumeInfo.imageLinks.thumbnail
                    }
                  />
                </Card>
              </Card.Grid>
              <Card.Grid
                hoverable={false}
                style={{
                  width: 321,
                  height: 180,
                  padding: 0,
                }}
              >
                <Card
                  style={{
                    height: "100%",
                  }}
                >
                  <Card.Meta
                    style={{ margin: 14 }}
                    title={book.volumeInfo.title}
                    description={
                      book.volumeInfo.description
                        ? book.volumeInfo.description.substring(0, 80)
                        : (book.volumeInfo.description = "")
                    }
                  />
                  <div
                    style={{
                      margin: 14,
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    {book.volumeInfo.authors && (
                      <span>
                        <b>Autor:</b> {book.volumeInfo.authors}
                      </span>
                    )}

                    <div>
                      <ReachableContext.Provider value="Light">
                        <Button
                          onClick={() => {
                            modal.info(config);
                          }}
                          type="primary"
                        >
                          Adicionar
                        </Button>
                        {/* `contextHolder` should always under the context you want to access */}
                        {contextHolder}
                        {/* Can not access this context since `contextHolder` is not in it */}
                      </ReachableContext.Provider>
                    </div>
                  </div>
                </Card>
              </Card.Grid>
            </Card>
          );
        })}
      </div>
    </div>
  );
};

export default withRouter(CardBooksVisitor);
