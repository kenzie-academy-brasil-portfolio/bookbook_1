import React from "react";
import "antd/dist/antd.css";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CardBookWishlist from "./cardBookWishlist/CardBookWishlist";
import CardBookReading from "./cardBookReading/CardBookReading";
import CardBookReaded from "./cardBookReaded/CardBookReaded";
import "./BookShelf.css";
import { addWishlist, addReading, addReaded } from "../../actions";

class BookShelf extends React.Component {
  fetchWishlist = () => {
    const userId = JSON.parse(localStorage.getItem("user")).id;
    const url = `https://ka-users-api.herokuapp.com/users/${userId}/books`;

    const { dispatchWishlist, dispatchReading, dispatchReaded } = this.props;

    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("currentToken"),
      },
    })
      .then((res) => res.json())
      .then((data) => {
        const filterWishlist = data.filter((book) => book.shelf === 1);
        dispatchWishlist(filterWishlist);

        //Outro dispatch
        const filterReading = data.filter((book) => book.shelf === 2);
        dispatchReading(filterReading);

        //Outro dispatch
        const filterReaded = data.filter((book) => book.shelf === 3);
        dispatchReaded(filterReaded);
      });
  };

  componentDidMount() {
    this.fetchWishlist();
  }

  render() {
    return (
      <div className="Estante">
        <div className="BookShelfGeneric">
          <h1 style={{ display: "inline-block", width: 1040, height: 40 }}>
            <b>Lista de leitura</b>
          </h1>
          <CardBookWishlist />
        </div>

        <div className="BookShelfGeneric">
          <h1 style={{ display: "inline-block", width: 1040, height: 40 }}>
            <b>Estou lendo</b>
          </h1>
          <CardBookReading />
        </div>
        <div className="BookShelfGeneric">
          <h1 style={{ display: "inline-block", width: 1040, height: 40 }}>
            <b>Lidos</b>
          </h1>
          <CardBookReaded />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  wishlist: state.books.wishlist,
  reading: state.books.reading,
  readed: state.books.readed,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchWishlist: (wishlist) => dispatch(addWishlist(wishlist)),
  dispatchReading: (reading) => dispatch(addReading(reading)),
  dispatchReaded: (readed) => dispatch(addReaded(readed)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(BookShelf));
