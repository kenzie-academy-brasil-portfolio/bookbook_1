import React from "react";
import { connect } from "react-redux";
import { Card, Button, Modal, Input, Rate } from "antd";
import { updateBookReview } from "../../../actions";

const { TextArea } = Input;

class CardBookReaded extends React.Component {
  state = {
    visible: false,
    bookTitle: "",
    bookID: "",
    bookRate: "",
    bookReview: "",
    book: [],
    bookKey: null,
  };

  handleChange = (bookRate) => {
    this.setState({ bookRate });
  };

  handleReview = (e) => {
    this.setState({ bookReview: e.target.value });
  };

  showModal = (key, book) => {
    this.setState({
      bookReview: book.review,
      visible: true,
      bookTitle: book.title,
      bookID: book.id,
      bookKey: key,
      bookRate: book.grade,
    });
  };

  handleOk = () => {
    const idUser = JSON.parse(localStorage.getItem("user")).id;
    const { bookID, bookKey, bookReview, bookRate } = this.state;
    const urlApi = `https://ka-users-api.herokuapp.com/users/${idUser}/books/${bookID}`;
    const data = {
      book: {
        review: bookReview,
        grade: bookRate,
      },
    };

    fetch(urlApi, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("currentToken"),
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((book) =>
        this.props.updateBookReview(bookKey, bookReview, bookRate)
      );

    this.setState({
      visible: false,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <>
        {this.props.readed.map((book, key) => {
          return (
            <Card
              loading={false}
              hoverable
              style={{ width: 441, height: 180, marginTop: 20 }}
              key={key}
            >
              <Card.Grid
                hoverable={false}
                style={{
                  width: 120,
                  height: 180,
                  padding: 0,
                  overflow: "hidden",
                }}
              >
                <img
                  style={{ height: 180 }}
                  alt="capa do livro"
                  src={book.image_url}
                />
              </Card.Grid>

              <Card.Grid
                hoverable={false}
                style={{
                  width: 320,
                  height: 180,
                  padding: 0,
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Card.Meta
                  style={{ width: "100%", margin: 0, padding: 14 }}
                  title={book.title}
                  description={book.review}
                />
                <Button
                  type="primary"
                  style={{
                    width: "91%",
                    height: 36,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    marginBottom: 14,
                  }}
                  onClick={() => this.showModal(key, book)}
                >
                  Avaliar
                </Button>
              </Card.Grid>
            </Card>
          );
        })}
        <Modal
          title={`Livro: ${this.state.bookTitle}`}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <h4 style={{ marginTop: 0 }}>Conte-nos o que achou desse livro.</h4>
          <TextArea
            placeholder="Gostei desse livro porquê..."
            rows={4}
            style={{ marginTop: 6 }}
            onChange={this.handleReview}
            value={this.state.bookReview}
          />
          <Rate
            style={{ marginTop: 12 }}
            value={this.state.bookRate}
            onChange={this.handleChange}
          />
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  readed: state.books.readed,
});

const mapDispatchToProps = (dispatch) => ({
  updateBookReview: (key, review, grade) =>
    dispatch(updateBookReview(key, review, grade)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CardBookReaded);
