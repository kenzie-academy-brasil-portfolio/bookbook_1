import React from "react";
import "antd/dist/antd.css";
import "./Login.css";
import { Input, Button, Form, notification } from "antd";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  EyeInvisibleOutlined,
  EyeTwoTone,
  UserOutlined,
  LockOutlined,
} from "@ant-design/icons";

import { login, userDatas } from "../../actions";
import { errorRules } from "./validator/validator";

const { Password } = Input;
const { Item } = Form;

class Login extends React.Component {
  state = {
    input: {
      user: "",
      password: "",
    },
  };

  propsDinamicas = (field, name) => ({
    style: { height: 54 },
    placeholder: name,
    value: this.state.input[field].value,
    onChange: (e) => this.setField(e, field),
  });

  setField = (e, field) => {
    this.setState({ input: { ...this.state.input, [field]: e.target.value } });
  };

  showPassword = (visible) => {
    return visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />;
  };

  handleClick = () => {
    const { user, password } = this.state.input;

    fetch("https://ka-users-api.herokuapp.com/authenticate", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        user,
        password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (!data.auth_token) {
          return notification.error(
            {
              message: "Login inválido",
              description:
                "Verifique os campos de usuário e senha e tente novamente",
            },
            2.5
          );
        }
        this.props.authentication(data.auth_token);
        this.props.user(data.user);

        localStorage.setItem("currentToken", data.auth_token);
        localStorage.setItem("user", JSON.stringify(data.user));
        this.props.login();
      });
  };

  render() {
    return (
      <div className="LoginPage">
        <Form
          layout="vertical"
          name="normal_login"
          style={{ width: 500, padding: 50 }}
        >
          <Item name="username" label="USUÁRIO" rules={errorRules.user}>
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              {...this.propsDinamicas("user", "USUÁRIO")}
            />
          </Item>
          <Item name="password" label="SENHA" rules={errorRules.password}>
            <Password
              {...this.propsDinamicas("password", "SENHA")}
              prefix={<LockOutlined className="site-form-item-icon" />}
              iconRender={this.showPassword}
            />
          </Item>

          <Item>
            <Button
              type="primary"
              size="large"
              htmlType="submit"
              onClick={() => this.handleClick()}
              style={{
                marginTop: 20,
                marginBottom: 6,
                height: 64,
                width: "100%",
                borderRadius: 4,
              }}
            >
              ENTRAR
            </Button>
            <p>
              <Link to="/forms">Registre-se   </Link> ou entre como
                      <Link to="/visitor">   Visitante</Link></p>
          </Item>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.login.user,
  authentication: state.login.authentication,
});

const mapDispatchToProps = (dispatch) => ({
  authentication: (authentication) => dispatch(login(authentication)),
  user: (user) => dispatch(userDatas(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));
