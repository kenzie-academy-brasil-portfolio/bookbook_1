import { 
  LOGIN_AUTHENTICATION,
  USER_DATAS
} from "../actions/actionTypes";

const defaultState = {
  user: "",
  authentication: "",
};

const login = (state = defaultState, action) => {
  switch (action.type) {
    case LOGIN_AUTHENTICATION:
      const { authentication } = action;
      return {
        ...state,
        authentication,
      };
    case USER_DATAS:
      const { user } = action;
      return {
        ...state,
        user,
      };

    default:
      return state;
  }
};

export default login;
